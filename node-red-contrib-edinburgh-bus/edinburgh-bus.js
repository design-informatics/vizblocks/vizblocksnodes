module.exports = function(RED) {
    "use strict";
    var request = require("request");
    const url = "https://tfeapp.com/api/website/stop_times.php?stop_id=";

    function EdinburghBusNode(config) {
        RED.nodes.createNode(this,config);
        this.stop = config.stop;
        this.service = config.service;
        this.stop_plaintext = config.stop_plaintext;
        this.interval_id = null;
        var node = this;

        this.interval_id = setInterval(function() {
              node.emit("input", {});
            }, 10000);

        node.on('input', function(msg) {
            var stop = JSON.parse(node.stop).id
            var options = { method: 'GET', url: url+stop};

            request(options, function (err, response, body) {
              if(err){
                  if(err.code === 'ETIMEDOUT' || err.code === 'ESOCKETTIMEDOUT') {
                      node.error(RED._("common.notification.errors.no-response"), msg);
                      node.status({fill:"red", shape:"ring", text:"common.notification.errors.no-response" + " - check internet connection"});
                  }else{
                      node.error(err,msg);
                      node.status({fill:"red", shape:"ring", text:"check internet connection"});
                  }
                  msg.payload = err.toString() + " : " + url;
                  msg.statusCode = err.code;
                  node.send(msg);
              }
              else{
                try { var data = JSON.parse(body); } // obj
                catch(e) { node.warn(RED._("httpin.errors.json-error")); }
                if (data){
                  var services = data.services;
                  var selected_service = services.filter(service => {
                    return service.service_name === node.service;
                  });
                  if (typeof selected_service[0] != 'undefined') {
                    node.status({fill:"green", shape:"dot", text:node.service+" from "+node.stop_plaintext});
                    msg.payload = selected_service[0].first_departure;
                  }else {
                    node.status({fill:"red", shape:"dot", text:node.service+" from "+node.stop_plaintext});
                    msg.payload = null;
                  }
                }else{
                  msg.payload = "no data found for the selected service";
                }
                node.send(msg);
              }
            });
        });

    }
    RED.nodes.registerType("edinburgh-bus",EdinburghBusNode);

    EdinburghBusNode.prototype.close = function() {
        if (this.interval_id != null) {
            clearInterval(this.interval_id);
            if (RED.settings.verbose) { this.log(RED._("inject.stopped")); }
          }
    };
}
