module.exports = function(RED) {
    "use strict";
    var request = require("request");
    const urlStation = "http://gbfs.urbansharing.com/edinburghcyclehire.com/station_status.json";
    const urlService = "http://gbfs.urbansharing.com/edinburghcyclehire.com/system_metrics.json";

    function JustEatCyclesNode(config) {
        RED.nodes.createNode(this,config);
        this.source = config.source;
        this.station = config.station;
        this.service_data = config.service_data;
        this.station_data = config.station_data;
        this.station_plaintext = config.station_plaintext;
        this.station_data_plaintext = config.station_data_plaintext;
        this.service_data_plaintext = config.service_data_plaintext;
        this.interval_id = null;
        var node = this;

        this.interval_id = setInterval(function() {
              node.emit("input", {});
            }, 10000);

        node.on('input', function(msg) {
          var url = (node.source === "station") ? urlStation : urlService;
          var options = { method: 'GET',
            url: url,
            headers: {'Client-Identifier': 'uoe-wallvis' }};

          request(options, function (err, response, body) {
            if(err){
                if(err.code === 'ETIMEDOUT' || err.code === 'ESOCKETTIMEDOUT') {
                    node.error(RED._("common.notification.errors.no-response"), msg);
                    node.status({fill:"red", shape:"ring", text:"common.notification.errors.no-response" + " - check internet connection"});
                }else{
                    node.error(err,msg);
                    node.status({fill:"red", shape:"ring", text:"check internet connection"});
                }
                msg.payload = err.toString() + " : " + url;
                msg.statusCode = err.code;
                node.send(msg);
            }
            else{
              try { msg.payload = JSON.parse(body); } // obj
              catch(e) { node.warn(RED._("httpin.errors.json-error")); }

              if (node.source === "station" && node.station && node.station_data){
                  var stations = msg.payload.data.stations;
                  msg.payload = stations.filter(station => {
                      return station.station_id === node.station;
                  });
                  msg.payload = msg.payload[0][node.station_data];
                  node.status({fill:"green", shape:"dot", text:node.station_data_plaintext+" at "+node.station_plaintext});
              } else if (node.source === "service" && node.service_data) {
                  msg.payload = msg.payload.data[node.service_data];
                  node.status({fill:"green", shape:"dot", text:node.service_data_plaintext});
              } else {
                  msg.payload = "error: select a data source"
              }
              node.send(msg);
            }
          });
          
        });

    }
    RED.nodes.registerType("justeat-cycles",JustEatCyclesNode);

    JustEatCyclesNode.prototype.close = function() {
        if (this.interval_id != null) {
            clearInterval(this.interval_id);
            if (RED.settings.verbose) { this.log(RED._("inject.stopped")); }
          }
    };
}
